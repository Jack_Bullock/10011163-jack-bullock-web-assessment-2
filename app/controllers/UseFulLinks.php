<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Usefullinks extends Controller {

        public function __construct() {
            $this->DataBase = $this->model('DataBase2');
        }

        public function index() {
            
            $database = $this->DataBase->getUsefullinksPage();

            $database = [
                'Usefullinks' => $database
            ];

            $this->view('Usefullinks/index', $database);
        }
    }
?>