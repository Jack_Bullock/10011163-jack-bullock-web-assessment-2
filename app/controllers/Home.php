<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Home extends Controller {

        public function __construct() {
            $this->DataBase = $this->model('DataBase2');
        }

        public function index() {
            
            $database = $this->DataBase->getHomePage();

            $database = [
                'Home' => $database
            ];

            $this->view('Home/index', $database);
        }
    }
 ?>