<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class AboutUs extends Controller {

        public function __construct() {
            $this->DataBase = $this->model('DataBase2');
        }

        public function index() {
            
            $database = $this->DataBase->getAboutUsPage();

            $database = [
                'AboutUs' => $database
            ];

            $this->view('AboutUs/index', $database);
        }
    }
?>