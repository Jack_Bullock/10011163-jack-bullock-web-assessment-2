<?php

    include(APPROOT . '/helper/helperfunctions.php');

    class Advice extends Controller {

        public function __construct() {
            $this->DataBase = $this->model('DataBase2');
        }

        public function index() {
            
            $database = $this->DataBase->getAdvicePage();

            $database = [
                'Advice' => $database
            ];

            $this->view('Advice/index', $database);
        }
    }
?>