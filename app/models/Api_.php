<?php

    class Api_ {

        private $db;

        public function __construct() {
            $this->db = new Database;
        }

        public function displayAllPeople() {
            $this->db->query('SELECT * FROM Appointment_requeste');
            return $this->db->showJSON();
        }

        public function displaySinglePeople($id) {
            $this->db->query('SELECT * FROM Appointment_request WHERE ID = :id');
            $this->db->bind(':id', $id);
            return $this->db->showJSON();
        }

        public function addPerson($fn, $ln, $dob) {

            $this->db->query('INSERT INTO  Appointment_request (FNAME, LNAME) VALUES (:fn, :ln)');

            $this->db->bind(':fn', $fn);
            $this->db->bind(':ln', $ln);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }

        }

        public function updatePerson($id, $fn, $ln) {

            $this->db->query('UPDATE Appointment_request SET FNAME = :fn, LNAME = :ln WHERE ID = :id');

            $this->db->bind(':id', $id);
            $this->db->bind(':fn', $fn);
            $this->db->bind(':ln', $ln);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }

        public function removePerson($id) {

            $this->db->query('DELETE FROM Appointment_request WHERE ID = :id');

            $this->db->bind(':id', $id);

            if($this->db->execute()) {
                return true;
            } else {
                return false;
            }
        }
    }

?>