Use containerdb;

DROP TABLE IF EXISTS Content;
DROP TABLE IF EXISTS Contact_Form;
DROP TABLE IF EXISTS Appointment_request;
--CREATE TABLE
CREATE TABLE  Content (
   CONTENTID INT(11)  NOT NULL auto_increment,
   PAGEIDENTIFIER varchar(255) NOT NULL,
   CONTENT TEXT NOT NULL,
   PRIMARY KEY (CONTENTID)
 ) AUTO_INCREMENT=1;

CREATE TABLE Contact_Form (
FORMID int(11) NOT NULL auto_increment,
FNAME varchar(255) NOT NULL,
LNAME varchar(255) NOT NULL,
EMAIL varchar(255) NOT NULL,
PHONE int (11) NOT NULL,
QUESTION VARCHAR (255) NOT NULL,
PRIMARY KEY (formID)
)AUTO_INCREMENT=1;

CREATE TABLE Appointment_request (
ID int(11) NOT NULL auto_increment,
FNAME varchar(255) NOT NULL,
LNAME varchar(255) NOT NULL,
EMAIL varchar(255),
PHONE int (11),
_Subject VARCHAR (255),
TD VARCHAR (255) NOT NULL,
PRIMARY KEY (RequestID)
)AUTO_INCREMENT=1;

CREATE TABLE IMAGES(
IMAGES varchar(255) NOT NULL
)

CREATE Table posts (
    ID INT(11) AUTO_INCREMENT,
    TITLE VARCHAR(255) NOT NULL,
    CONTENT TEXT(500),
    PRIMARY KEY (Id)
)
-- CREATE DATA
INSERT INTO Content (PAGEIDENTIFIER,CONTENT) VALUES ('Home','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing');
INSERT INTO Content (PAGEIDENTIFIER,CONTENT) VALUES ('Contact page','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur');
INSERT INTO Content (PAGEIDENTIFIER,CONTENT) VALUES ('About us','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer aliquam adipiscing lacus.');
INSERT INTO Content (PAGEIDENTIFIER,CONTENT) VALUES ('Advice','Lorem ipsum dolor sit');
INSERT INTO Content (PAGEIDENTIFIER,CONTENT) VALUES ('Useful links','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur');
INSERT INTO Content (PAGEIDENTIFIER,CONTENT) VALUES ('Request page','Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Curabitur sed tortor. Integer');

INSERT INTO Contact_Form (FNAME,LNAME,EMAIL,PHONE,QUESTION) VALUES ('Reed','Castaneda','eleifend.nunc.risus@liberonecligula.co.uk','(03) 3390 5067','fringilla ornare placerat, orci lacus vestibulum');
INSERT INTO Contact_Form (FNAME,LNAME,EMAIL,PHONE,QUESTION) VALUES ('Caldwell','Page','nec@Morbisit.com','(02) 7792 0142','sem egestas blandit. Nam nulla');
INSERT INTO Contact_Form (FNAME,LNAME,EMAIL,PHONE,QUESTION) VALUES ('Solomon','Sanford','lorem.vitae.odio@tellusNunc.edu','(04) 2044 0897','rhoncus.');
INSERT INTO Contact_Form (FNAME,LNAME,EMAIL,PHONE,QUESTION) VALUES ('Octavius','Blake','Curabitur.vel.lectus@elementumat.ca','(09) 9647 3388','pharetra. Nam ac');
INSERT INTO Contact_Form (FNAME,LNAME,EMAIL,PHONE,QUESTION) VALUES ('Chaney','Boone','sit.amet.lorem@inconsequatenim.org','(03) 6209 7642','ante');
INSERT INTO Contact_Form (FNAME,LNAME,EMAIL,PHONE,QUESTION) VALUES ('Herrod','Beasley','sodales.purus.in@acfeugiatnon.ca','(08) 0848 4057','Nunc mauris. Morbi');

INSERT INTO Appointment_request (FNAME,LNAME,EMAIL,PHONE,_Subject,TD) VALUES ('Reed','Castaneda','eleifend.nunc.risus@liberonecligula.co.uk','(03) 3390 5067','fringilla ornare placerat, orci lacus vestibulum','May 23rd, 2018');
INSERT INTO Appointment_request (FNAME,LNAME,EMAIL,PHONE,_Subject,TD) VALUES ('Caldwell','Page','nec@Morbisit.com','(02) 7792 0142','sem egestas blandit. Nam nulla','October 5th, 2017');
INSERT INTO Appointment_request (FNAME,LNAME,EMAIL,PHONE,_Subject,TD) VALUES ('Solomon','Sanford','lorem.vitae.odio@tellusNunc.edu','(04) 2044 0897','rhoncus.','December 17th, 2018');
INSERT INTO Appointment_request (FNAME,LNAME,EMAIL,PHONE,_Subject,TD) VALUES ('Octavius','Blake','Curabitur.vel.lectus@elementumat.ca','(09) 9647 3388','pharetra. Nam ac','September 25th, 2018');
INSERT INTO Appointment_request (FNAME,LNAME,EMAIL,PHONE,_Subject,TD) VALUES ('Chaney','Boone','sit.amet.lorem@inconsequatenim.org','(03) 6209 7642','ante','August 1st, 2018');
INSERT INTO Appointment_request (FNAME,LNAME,EMAIL,PHONE,_Subject,TD) VALUES ('Herrod','Beasley','sodales.purus.in@acfeugiatnon.ca','(08) 0848 4057','Nunc mauris. Morbi','March 28th, 2018');

INSERT INTO IMAGES (IMAGES) VALUES ('Image1');
INSERT INTO IMAGES (IMAGES) VALUES ('Image2');
INSERT INTO IMAGES (IMAGES) VALUES ('Image3');
INSERT INTO IMAGES (IMAGES) VALUES ('Image4');
INSERT INTO IMAGES (IMAGES) VALUES ('Image5');

INSERT INTO Posts (TITLE, CONTENT) VALUES ('title 1', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');
INSERT INTO Posts (TITLE, CONTENT) VALUES ('title 2', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');
INSERT INTO Posts (TITLE, CONTENT) VALUES ('title 3', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');
INSERT INTO Posts (TITLE, CONTENT) VALUES ('title 4', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');
INSERT INTO Posts (TITLE, CONTENT) VALUES ('title 5', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit.');
